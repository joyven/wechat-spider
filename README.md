# wechat-spider

本项目基于[smart-boot](https://gitee.com/smartboot/smart-boot)脚手架搭建，具体使用手册也和smartboot文档保持一致。

文档说明参见[http://smartboot.oschina.io/smart-boot/](http://smartboot.oschina.io/smart-boot/)

## 更新日志
#### 版本号：1.0.2  
- 更新日期：2017-01-06  
- 更新内容：
	1. spring boot升级至1.4.3版本
	2. smart sosa升级至1.0.4
	3. 新增内容转换器

#### 版本号：1.0.1  
- 更新日期：2016-11-17  
- 更新内容：
	1. spring boot升级至1.4.2版本
	2. smart sosa升级至1.0.3
	3. 升级第三方依赖包
	4. 默认启用dbcp2连接池
	  