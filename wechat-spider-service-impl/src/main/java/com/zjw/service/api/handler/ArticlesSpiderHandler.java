package com.zjw.service.api.handler;

import com.zjw.service.api.ApiAuthBean;
import com.zjw.service.api.RestApiHandler;
import org.springframework.util.CollectionUtils;

import java.util.Map;
import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * Date: 2018/1/8
 * Time: 上午10:24
 * Description:
 *
 * @author zhoujunwen
 * @version 1.0
 */
public class ArticlesSpiderHandler implements RestApiHandler {
    @Override
    public Object execute(ApiAuthBean authBean, Map<String, String> params) {
        if (!CollectionUtils.isEmpty(params)) {
            params.forEach((k, v) -> System.out.println(k + ":" + v));
        }
        return null;
    }

    @Override
    public boolean needTransaction(ApiAuthBean authBean, Map<String, String> params) {
        return false;
    }
}
